<h1 align=center>
Добро пожаловать в наш уютный тред!
</h1>
<h1 align=center>
<img src="img/skuf.png">
</h1>

<h1 align=center>
<img src="img/bankomat.jpg">
</h1>

>СЛЫШ `БАНКОМАТ` ЁПТА CANDY CRUSH МНЕ НУЖЕН ТЫ ПОНЯЛ? ТЫ `БАНКОМАТ` ЕСЛИ НЕ ОТКРЫВАЕШЬ КАРТИНКИ ПРИЛОЖЕНИЕМ ФОТО, НЕТ ЭТО НЕ ПРАВДА ЧТО ОНА ДОЛГА ЗАГРУЖАЕТСЯ, ЧТО? ТЫ НЕ СЛУШАЕШЬ МУЗЫКУ ЧЕРЕЗ GROOVE? А КАК ЖЕ КОЛЬКУЛЯТОР? ТЫ ЧТО ПОСТАВИЛ СТАРЫЙ ИЗ СПЕРМЫ? НО ВЕДЬ ХОРОШО РАБОТАЕТ И ОН С ЭФФЕКТАМИ И ДОЛГО ОН НЕ ГРУЗИТСЯ. ТЫ ЧТО НЕ ПОЛЬЗУЕШЬСЯ ФОТО 3D? НО ВЕДЬ ТАМ МОЖНО РАЗНУЮ ГРАФИКУ ДЕЛАТЬ ТЫ ЧЁ `БАНКОМАТ`? ТЫ ЧТО ВСЁ УДАЛИЛ? UWP РЕВОЛЮЦИЯ А ТЫ `БАНКОМАТ` ВСЁ УДАЛЯЕШЬ? ТЫ ЧТО КАКАЯ НАГРУЗКА ВЕДЬ ОНА UWP ОНА НЕ МОЖЕТ НАГРУЖАТЬ, ТЫ ЧТО И СТОР УДАЛИЛ? И КАК ТЫ БУДЕШЬ CANDY CRUSH ОБНОВЛЯТЬ `БАНКОМАТ`?

Краткое содержание:
- [Что ставить?](#что-ставить)
	- ["Consumer"? "Business"? Что это вообще такое?](#consumer-business-что-это-вообще-такое)
	- [У меня есть старый ноутбук с лицензионной Семёркой, можно как-то использовать её ключ?](#у-меня-есть-старый-ноутбук-с-лицензионной-семёркой-можно-как-то-использовать-её-ключ)
---
- [Хочу отключить встроенный антивирус - "Windows Defender"](#хочу-отключить-встроенный-антивирус---windows-defender)
	- [Блокируем системе задачи для проверки состояния дефендера (не обязательно):](#блокируем-системе-задачи-для-проверки-состояния-дефендера-не-обязательно)
- [Хочу "оптимизировать" мою систему](#хочу-оптимизировать-мою-систему)
	- [Службы/Групповые политики?](#службыгрупповые-политики)
	- [Хочу выставить на минимум телеметрию через групповые политики](#хочу-отключить-выставить-на-минимум-телеметрию-через-групповые-политики)
- [Хочу отключить обновления](#хочу-отключить-обновления)
	- [Установка вручную обновлений (только которые вам надо)](#установка-вручную-обновлений-только-которые-вам-надо)
- [Хочу отключить службу 'SysMain'](#хочу-отключить-службу-sysmain)
- [Хочу отключить индексацию диска](#виндовс-постоянно-индексирует-диск-тем-самым-нагружая-его-что-делать)
- [Хочу удалить 'Onedrive'](#хочу-удалить-onedrive)
- [Хочу удалить 'Edge'](#хочу-удалить-edge)
- [Хочу отключить гибернацию](#хочу-отключить-гибернацию)
- [Хочу поставить/обновить драйвера системы](#хочу-поставитьобновить-драйвера-системы)
- [Хочу перед установкой виндовс очистить диск от всего и ещё поменять структуру раздела](#хочу-перед-установкой-виндовс-очистить-диск-от-всего-и-ещё-поменять-структуру-раздела)
- [Хочу удалить/установить/активировать ключи виндовса](#хочу-удалитьустановитьактивировать-ключи-виндовса)
- [Хочу узнать мой оригинальный ключ виндовс](#хочу-узнать-мой-оригинальный-ключ-виндовс)
- [Хочу иметь журнал буфера обмена](#хочу-иметь-журнал-буфера-обмена)
- [Хочу делать скрины экрана легко и просто нажимая одну кнопку](#хочу-делать-скрины-экрана-легко-и-просто-нажимая-одну-кнопку)
- [Хочу убрать мыльность во всех приложениях](#хочу-убрать-мыльность-во-всех-приложениях)
- [Хочу чтобы система не выходила из сна из за служб и прочих "таймеров пробуждения"](#хочу-чтобы-система-не-выходила-из-сна-из-за-служб-и-прочих-таймеров-пробуждения)
- [Хочу отключить процесс "Game Bar Presence Writer"](#хочу-отключить-процесс-game-bar-presence-writer)
---
- [Хочу скачать ЛТСЦ с официанального сайта](#хочу-скачать-лтсц-с-официанального-сайта)
- [Образы LTSC - IoT Enterprise LTSC](#образы-ltsc---iot-enterprise-ltsc)
- [Хочу установить библиотеки для ЛТСЦ чтобы устанавливать разные UWP приложения (Windows terminal)](#хочу-установить-библиотеки-для-лтсц-чтобы-устанавливать-разные-uwp-приложения-windows-terminal)
---
- [ОФИС ОФИС ОФИС ХАЧУ ЫЫЫЫЫЫ СРЕНЬК ПУУУУУУК](#офис-офис-офис-хачу-ыыыыыы-среньк-пуууууук)
---
- [Сборочки хуёрочки?](#сборочки-хуёрочки)
---
- [Активация](#активация)
- [Скачать](#скачать)
- [Дампы хешей](#дампы-хешей)
- [Как создать загрузочный образ?](#как-создать-загрузочный-образ)
---
- [Полезное](#полезное)
- [ПО по вкусу](#по-по-вкусу)
---
- [Установка Windows 10](#установка-windows-10)


# FAQ
## Что ставить?
Если у тебя есть лицензионный ключ от любой редакции 7, то обновись на ту же редакцию 10. (7 Ultimate = 10 Pro), 10 тоже будет лицензионной.
>"Короче блядь, какую версию скачать чтобы не было много лишнего гавна?"
>>`Образовательная` или `ЛТСЦ` другой выбор просто разнообразие кала.
- **Домашняя**

Кал из жопы. Полно рекламы и разных предустановленных приложений а ля кэндикрэш, пользоваться этой себя не уважать. Нету возможности изменять [групповые политики](https://docs.microsoft.com/ru-ru/troubleshoot/windows-client/group-policy/use-settings-app-group-policy). Оставлять если тебе лень что то менять и ты хотя бы минимально не вдупляешь за активаторы.

- **Профессиональная**

Групповые политики есть, то же самое, что и домашняя. [Ссылка на кэндикрэш](https://www.microsoft.com/ru-ru/p/candy-crush-saga/9nblggh18846#activetab=pivot:overviewtab)

- **Образовательная**

Оптимальная по соотношению между количеством рекламы и возможного контроля над системой. **`Рядовому анону, которому нужна система под домашние нужды это самое то.`**

- **Корпоративная**

Не имеет никакого смысла. Имеет те же самые качества образовательный, но по дефолту включены разные корпоративные функции. Тот же самый цикл поддержки обновлениями как и образовательная.

- **Банкоматная**

Групповые политики есть. Никаких предустановленных UWP приложений (системные да). Реклама в ноль. LTSC 2021 поддерживает все игры и можно вернуть Microsoft Store. Главное преимущество это поддержка обновлениями безопасности на 10 лет. Выдаёт деньги.

> **Note**
> **LTSC IoT Enterprise 2021 [21H2] - LTSC 2018 [1809] *МОЖНО* активировать с HWID *ИЛИ* KMS_VL_ALL.**

> **Note**
> **LTSC 2021 [21H2] *ТОЛЬКО* с KMS_VL_ALL.**

> **Note**
> **Срок поддержки обновлениями безопасности: LTSB 2016 10 лет - LTSC 2018 10 лет - LTSC 2021 только 5 лет - LTSC IoT Enterprise 2021 10 лет.**

- **`[1607]LTSB/[1809-21H2] LTSC Имеют тот же самый выбор обновлений безопасности (Security Updates) и пакетов возможностей (Feature Updates) что и на других редакциях`**.

- **`[21H2]LTSC от [21H2] LTSC IoT Enterprise отличается только сроком поддержки обновлениями и НИЧЕМ больше.`**

- **`IoT Enterprise [НЕ ПУТАТЬ С LTSC] отличается от [21H2] LTSC IoT Enterprise датой окончания поддержки, обновлениями безопасности и типом предустановленных приложений.`**

- **`Microsoft Store и все его сопутствующие UWP приложения присутствуют во всех редакциях кроме LTSB[2016] - LTSC[2018/2021].`**

- **`Обновление редакций возможно на всех перечисленных версиях кроме LTSB/LTSC.`**

- **`Редакция "N" характерна отсутствием некоторых системных библиотек и предустановленных дефолтных приложений.`**

- **`Редакция "Pro Образовательная" базируется на "Профессиональной" версии и имеет её предустановленные характеристики, в то же самое время "Образовательная" базируется на "Корпоративной" и имеет предустановленные характеристики её образа.`**
	- [Microsoft Docs](https://learn.microsoft.com/ru-ru/education/windows/windows-editions-for-education-customers)

[Каналы обслуживания](https://docs.microsoft.com/ru-ru/windows/release-health/release-information#текущие-версии-windows-10-по-вариантам-обслуживания):
Версия | Окончание обслуживания: Домашняя, Pro, Pro для образовательных учреждений и Pro для рабочих станций | Окончание обслуживания: Корпоративная, для образовательных учреждений и IoT Корпоративная
------ | ------ | ------
22H2 | 2024-05-14 | 2025-05-13
21H2 | Окончание обслуживания | 2024-06-11

Выпуски **Корпоративная** и **IoT Корпоративная LTSB/LTSC**:
Версия | Дата окончания основной фазы поддержки | Дата окончания расширенной поддержки
------ | ------ | ------
21H2 `[LTSC 2021]` | 2027-01-12 | 2032-01-13 (Только IoT Корпоративная)
1809 `[LTSC 2018]` | 2024-01-09 | 2029-01-09
1607 `[LTSB 2016]` | Окончание обслуживания | 2026-10-13

## "Consumer"? "Business"? Что это вообще такое?
Разница только в присутствии издания "Домашняя" и в отсутствии [универсального ключа многократной установки (GVLK)](https://learn.microsoft.com/ru-ru/windows-server/get-started/kms-client-activation-keys#generic-volume-license-keys-gvlk) в `Consumer`, в `Business` нету "Домашнего" издания, но присутствует GVLK ключ.

`Consumer`:
- **Домашняя**
- **Профессиональная** 
- **Образовательная**
- **(и её варианты "N")**

`Business`:
- **Профессиональная** 
- **Образовательная**
- **Корпоративная**
- **(и её варианты "N")**

## У меня есть старый ноутбук с лицензионной Семёркой, можно как-то использовать её ключ?
- Да, можно. Ноутбуки с наклейками ключей — один из способов получения халявной лицензии, часто это Pro, если лицензия была на Ultimate.

---

## Хочу отключить встроенный антивирус - "Windows Defender"
> **Note**
> Антивирус "Defender" автоматически удаляет разные активаторы, "кряки" и так далее.

> **Note**
> [Альтернативная утилита для отключение дефендера.](https://github.com/ionuttbara/windows-defender-remover)

Единственный "нормальный" способ это отключение через групповые политики и через "[RunAsTI](https://github.com/AveYo/LeanAndMean/blob/main/RunAsTI.bat)". Нажми клавиши WinR на клавиатуре и введи gpedit.msc

В открывшемся редакторе локальной групповой политики переходи к разделу:
- "Конфигурация компьютера" - "Административные шаблоны" - "Компоненты Windows" - "Антивирусная программа Microsoft Defender"

- [x]	Включаешь пункт "Выключить антивирусную программу Microsoft Defender" (именно так — "Включено" отключит антивирус).

Заходишь в папку:
- [x] "Защита в реальном времени" и включаешь "Выключить защиту в реальном времени".

- [x] "Клиентский интерфейс" - "Отключить все уведомления" и включить параметр.

Отключаем "SmartScreen":
- [x] "Конфигурация компьютера" - "Административные шаблоны" - "Компоненты Windows" - "SmartScreen Защитника Windows" в обеих папках отключаем "Настроить функцию SmartScreen Защитника Windows".

> **Warning**
> Два обязательных метода на ВЫБОР для дальнейшего отключения дефендера:
## Отключаем через реестр без отключения служб

1. Открываем `"Безопасность Windows"`
2. Открываем слева вкладку `"Защита от вирусов и угроз"`
3. Клик по `"Управление настройками"` снизу от `"Параметры защиты от вирусов и других угроз"`
4. Отключаем `"Защита от подделки"`
5. Скачиваем [ToggleDefender.bat](https://raw.githubusercontent.com/AveYo/LeanAndMean/main/ToggleDefender.bat) - [сурцы](https://github.com/AveYo/LeanAndMean/blob/main/ToggleDefender.bat)
6. `"CTRLA"` (Ф на клавиатуре) потом `"CTRLC"`
7. Вставляем в новый файл с тем же расширением `"CTRLV"` (.bat - .cmd - .ps1)

Или можно так:

![](img/githubdownload.png)

Или так:

![](img/githubsave.png)

8. Запускаем с [RunAsTI.bat](https://raw.githubusercontent.com/AveYo/LeanAndMean/main/RunAsTI.bat)
---

## Отключения всех служб и процессов
1. Открываем `"Безопасность Windows"`
2. Открываем слева вкладку `"Защита от вирусов и угроз"`
3. Клик по `"Управление настройками"` снизу от `"Параметры защиты от вирусов и других угроз"`
4. Отключаем `"Защита в режиме реального времени"`

![](img/defender.png)

5. Скачиваем [RunAsTI.bat](https://raw.githubusercontent.com/AveYo/LeanAndMean/main/RunAsTI.bat) - [сурцы](https://github.com/AveYo/LeanAndMean/blob/main/RunAsTI.bat)
	- `"CTRLA"` (Ф на клавиатуре) потом `"CTRLC"`
	- Вставляем в новый файл с тем же расширением `"CTRLV"` (.bat - .cmd - .ps1)
->	`Или смотрим сверху`
6. Открываем командную строку от имени администратора
7. Переходим по пути, где находится `RunAsTI.bat` (например: `cd C:\Users\%имя_пользователя%\Desktop`)
8. Пишем: `RunAsTI.bat cmd`
9. **`ЖДЁМ ПОКА ОТКРОЕТСЯ НОВОЕ ОКНО КОМАНДНОЙ СТРОКИ АДМИНИСТРАТОРА`**

![](img/runasti.png)

10. В открывшейся новой командной строке копируем:
[Defender.bat](https://paste.gg/p/anonymous/20b41fc977e04ab2bddd6ba0260952a4/files/b176365944ef45eb931f96205b65e435/raw)
11. Перезагружаем


---

- Если нужно всё вернуть обратно:
[RestoreDefender.bat](https://paste.gg/p/anonymous/d9e20682525e4191a583fe1609392f00/files/ba1f6f17f884414d826ef2637d82ff56/raw)

## Блокируем системе задачи для проверки состояния дефендера (не обязательно):
1. Открываем командную строку от имени администратора
2. Вставляем:
```
takeown /A /F "C:\Windows\System32\Tasks\Microsoft\Windows\Windows Defender"
``` 
3. Открываем проводник и переходим по пути: `"C:\Windows\System32\Tasks\Microsoft\Windows\Windows Defender"`
4. На каждый из файлов нажимаем правой кнопкой и `"Свойства"` --- Заходим в вкладку `"Безопасность"` и нажимаем кнопку `"Изменить"`
5. В `"Группы или пользователи:"` выбираем `"СИСТЕМА"` и снизу ставим на запрет `"Чтение"/"Запись"`

![](img/defenderproprities.png)

## Хочу "оптимизировать" мою систему
Развлекайся, шизик:

Это на уже установленную систему:
- [privatezilla](https://github.com/builtbybel/privatezilla)
- [privacy.sexy](https://privacy.sexy)

Это на сам образ перед установкой:

> **Warning**
> **`Optimize-Offline работает только на виндовс 10/11. То есть, ты можешь создавать и модифицировать образ, только если ты уже сидишь на 10 или 11.`**
- [Optimize-Offline](https://github.com/gdeliana/Optimize-Offline)
## [Гайд по optimize-offline](https://codeberg.org/shapkodebil/optimizeofflineguide)

## Службы/Групповые политики?
~~Отключать службы это полная параша и адовый пиздец да бы шанс разьебать всё к хуям очень велик но и самая главная проблема это сделать всё универсально что бы всё работало у всех (нереально нахуй).~~

 [Ебитесь сами](https://rentry.org/ltsc#services)

Отключённые службы в моей системе:
- `17763.3770 (LTSC 1809)`

![](img/services.png)

Групповые политики - Конфигурация компьютера:
  - `17763.3770 (LTSC 1809)`

![](img/grouppolicies.png)

## Хочу отключить выставить на минимум телеметрию через групповые политики
> **Warning**
> Это работает на всех редакциях кроме домашней.

1. Нажми клавиши WinR на клавиатуре и введи gpedit.msc

В открывшемся редакторе локальной групповой политики переходи к разделу:
- "Конфигурация компьютера" - "Административные шаблоны" - "Компоненты Windows" - "Сборки для сбора данных и предварительные сборки"

- [x] Включаем пункт "Разрешить телеметрию" и выставляем параметр на 0.


## Хочу отключить обновления
>Нужны ли обновления винды или отключить их нафиг?
>>Обновления `безопасности` **нужны**, `предварительные`? **Нет**. Лучше ставить в-ручную каждый пакет один раз в месяц, всё описано ниже.
Нажми клавиши WinR на клавиатуре и введи gpedit.msc

В открывшемся редакторе локальной групповой политики переходи к разделу:
-  "Конфигурация компьютера" - "Административные шаблоны" - "Компоненты Windows" - "Центр обновления Windows"

-  Отключай пункт "Настройка автоматического обновления"/ "Configure Automatic Updates" (именно так — "Отключено" отключит обновления).

Если хочешь обновиться в центре, нажимаешь старт и всё пойдёт само.

Отключения поиска обновлений:
- "Конфигурация компьютера" - "Административные шаблоны" - "Компоненты Windows" - "Центр обновления Windows" 

- [x] Включить пункт "Указать размещение службы обновлений Майкрософт в интрасети" и указать в трех начальных полях: `""`

![](img/gpupdate.png)

---

Если необходимо, можно отключить и саму службу `"wuauserv"`(Центр обновления Windows):
[Следуем шагам](https://codeberg.org/shapkodebil/win10thread/#отключения-всех-служб-и-процессов)
1. Командная строка от имени администратора.
2. Вставляем `"cd C:\Users\%имя_пользователя%\Desktop"` или другая локация, где находится сам скрипт [RunAsTI.bat](https://raw.githubusercontent.com/AveYo/LeanAndMean/main/RunAsTI.bat).
3. Вставляем `"RunAsTI.bat cmd"` и ждём, когда откроется новая командная строка.
4. Вставляем для отключения службы:
```
reg add "HKLM\System\CurrentControlSet\Services\wuauserv" /v "Start" /t REG_DWORD /d "4" /f
```
Вернуть как было:
```
reg add "HKLM\System\CurrentControlSet\Services\wuauserv" /v "Start" /t REG_DWORD /d "3" /f
```

---

## Установка вручную обновлений (только которые вам надо)
-  [Пакеты обновления:](https://support.microsoft.com/ru-ru/topic/журнал-обновлений-windows-10-857b8ccb-71e4-49e5-b3f6-7073197d98fb) установка по двойному клику.
Конечно надо выбрать свою версию:`19045 - 22H2`/`19044 - 21H2`/`17763 - 1809`
Скролл до низа и нажми на "каталог Центра обновления Майкрософт."
`Предварительная версия` = Preview (Features Updates) / Все другие = `Обновления Безопасности` (Security Updates).
	- [Оновления для 22H2](https://www.catalog.update.microsoft.com/Search.aspx?q=22H2basedx641903)	
	- [Обновления для 21H2 - LTSC 2021](https://www.catalog.update.microsoft.com/Search.aspx?q=21H2basedx64LTSB)
	- [Обновление для 1809 - LTSC 2019](https://www.catalog.update.microsoft.com/Search.aspx?q=1809basedx64LTSB)

"Каталог Центра обновления Майкрософт":

![](img/wupdatecatalog.png)

"Загрузить":

![](img/wupdatecatalogdownload.png)
	
## Хочу отключить службу 'SysMain'
> **Note**
> Отключение службы может повысить производительность системы.

Земля стекловатой, надо купить ссд если хочется вменяемой отзывчивости системы.

Может помочь если всётаки решил посидеть на хдд:
- CtrlShiftEsc → Службы → Открыть службы → SysMain → Свойства → Тип запуска: Отключена.

> **Note**
> /SSD/ Тред в [/hw/](https://2ch.hk/hw/catalog.html) поможет

---
> **Note**
> [Документация (Windows Internals)](https://libgen.rocks/get.php?md5=ff0e6a9d867a3a6e9527eb1d946810fc&key=9FYKVX8X7PWH6TDY) Страница 472

Узнать, если Proactive memory management (`SuperFetch`) включён:
- Повершелл от имени администратора:
```
Get-MMAgent
``` 

![](img/Get-MMAgent.png)

1. `ApplicationLaunchPrefetching`
	[Префетч](https://ru.wikipedia.org/wiki/Prefetcher):"Запуск Windows и приложений сопровождается чтением в память и обработкой огромного количества файлов. Зачастую один и тот же файл открывается по несколько раз для чтения различных сегментов. Такой нерациональный доступ к файлам занимает много времени. Гораздо эффективнее обращаться к каждому файлу только один раз, загружая информацию в оперативную память ещё до того, как она станет нужна программам."

2. `ApplicationPrelaunch` [возможно](https://forums.guru3d.com/threads/a-bit-detailed-info-about-superfetch-and-cache-manager-in-windows-relying-on-memory-manager.419263/#post-5820566) предназначен только для UWP приложений.
Отключить:
```
Disable-MMAgent -ApplicationPrelaunch
```
3. `MemoryCompression`

	- Страница 449 (Windows Internals)

	![](img/MemoryCompression.png)

	В основном может помочь если используешь UWP приложения.
	Отключить: 
```
Disable-MMAgent -MemoryCompression
```

4. `PageCombining`
	- Страница 459 (Windows Internals)

	![](img/MemoryCombining.png)

	Пытается исправить проблему с дупликатоми *страниц* в озу.

## Виндовс постоянно индексирует диск тем самым нагружая его, что делать?
- Отключить службу "Windows Search" если не нужна индексация файлов.

## Хочу удалить 'Onedrive'
1. Открываем командную строку от имени администратора и вставляем:
- [x] [Onedrive.bat](https://paste.gg/p/anonymous/1911c0de7e9b4035bb98c2d66679ebe0/files/15f7a644c6b2494ea9fc7c2ac55d435a/raw)
## Хочу удалить 'Edge'
Перед запуском скриптов на всякий случай временно [отключаем дефендер](https://codeberg.org/shapkodebil/win10thread/#отключения-всех-служб-и-процессов) по первому пункту.

1. Открываем командную строку от имени администратора и вставляем:
```
reg delete HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\EdgeUpdate\ClientState\{56EB18F8-B008-4CBD-B6D2-8C97FE7E9062} /v experiment_control_labels /f 
```

**`Если ключ реестра с первого пункта не существует то просто продолжаем дальше.`**

```
PowerShell -ExecutionPolicy Unrestricted -Command "$installer = (Get-ChildItem "^""$env:ProgramFiles*\Microsoft\Edge\Application\*\Installer\setup.exe"^""); if (!$installer) {; Write-Host 'Could not find the installer'; } else {; & $installer.FullName -Uninstall -System-Level -Verbose-Logging -Force-Uninstall; }"
```

2. Скачиваем и запускаем от имени администратора
- [Edge_Removal.bat](https://raw.githubusercontent.com/AveYo/fox/main/Edge_Removal.bat)
	- [Сурцы Edge_Removal.bat](https://github.com/AveYo/fox/blob/main/Edge_Removal.bat)

Альтернативные методы, если у кого то не работает первый:
[Remove-Edge.bat](https://raw.githubusercontent.com/ShadowWhisperer/Remove-Edge-Chromium/main/Remove-Edge.bat)
[RemoveEdge.ps1](https://github.com/he3als/EdgeRemover/releases/download/v1.5.0/RemoveEdge.ps1)
[Remove-MS-Edge](https://github.com/ShadowWhisperer/Remove-MS-Edge)


## Хочу отключить гибернацию
1. Открываем командную строку от имени администратора и вставляем:
```
 powercfg /hibernate off
```
## Хочу поставить/обновить драйвера системы
- Центр обновлений
- Сайт производителя материнки/ноутбука
- [SDI Origin](https://www.glenn.delahoy.com/snappy-driver-installer-origin/)

> **Note**
> Для SDI Origin **`нет необходимости скачивать весь пакет драйверов`**, скачиваем только "Установщик драйверов Snappy, исходная версия x.xx.xx" и при запуске появится возможность скачать только те драйвера которые необходимы для вашей системе.

> **Warning**
> Не путать [SDI Origin](https://sourceforge.net/projects/snappy-driver-installer-origin/) с sdi-tool.

## Хочу перед установкой виндовс очистить диск от всего и ещё поменять структуру раздела
На экране начальной установки сразу после загрузки с флешки нажимаешь "Восстановление системы", ищи "Командная строка".
Вводим:
```winbatch
diskpart
list disk
select disk %номер_диска_где_хотим_всё_удалить%
clean
convert gpt/mbr
```

Если нужно удалить только раздел:

`1 - 2 - 3` сверху
```winbatch
list partition
delete partition %номер_раздела_который_хотим_удалить%
```
Перезапуск с флэшки и устанавливаешь всё как обычно.

## Хочу удалить/установить/активировать ключи виндовса
Запускаешь "Командная строка" от имени администратора:
```winbatch
#Удалить ключ:
slmgr /upk
#Удалить ключ из реестра:
slmgr /cpky
#Установка другого ключа:
slmgr /ipk xxxxx-xxxxx-xxxxx-xxxxx
#Онлайн активация виндовс с текущим ключом:
slmgr /ato
```
## Хочу узнать мой оригинальный ключ виндовс
1. Открываем командную строку от имени администратора
2. Вставляем:
```
powershell "(Get-WmiObject -query ‘select * from SoftwareLicensingService’).OA3xOriginalProductKey"
```

## Хочу иметь журнал буфера обмена
- [x] Комбинация `"WinV"`

![](img/cvbuffer.png)

## Хочу делать скрины экрана легко и просто нажимая одну кнопку
Заходишь в:
- "Настройки" - "Специальные возможности" - "Взаимодействие" - "Клавиатура" - Включить "Сочетание клавиш: PRINT SCREEN"

Теперь нажимаешь кнопку `"PrtSc"` включится редактор изображения и можно сделать скриншот только того участка экрана, который тебе нужен или весь экран.
Закинуть пикчу можно куда угодно просто нажав `"CtrlV"`.

---

- `В первых билдах` LTSC 2021-`[19044.1288]` есть проблема, где нужные библиотеки отсутствуют. Просто обновитесь на последний пакет обновлений безопасности. 

## Хочу убрать мыльность во всех приложениях
- Пуск - Параметры - Система - Дисплей - Дополнительные параметры масштабирования:
	- [x] Разрешить Windows исправлять размытость в приложениях
- Настраиваемое масштабирование:
	- `Обязательно ставите число с которым вам комфортнее именно здесь (100-500)`

## Хочу чтобы система не выходила из сна из за служб и прочих "таймеров пробуждения"
1. Пуск 
2. Папка "Служебные --- Windows" - "Панель управления"
4. Электропитание - "Настройка `текущей` схемы электропитания" - "Изменить дополнительные параметры питания"
5. "Сон" - "Разрешить таймеры пробуждения" - `Отключить`

![](img/wakeuptimers.png)

## Хочу отключить процесс "Game Bar Presence Writer"
>Процесс может помешать системе нормально уходить в сон
1. Открываем командную строку от имени администратора
2. Вставляем:
```
takeown /A /F "C:\Windows\System32\GameBarPresenceWriter.exe"
``` 
3. Открываем "Редактор реестра"
	- "WinR"  "regedit"
4. Идём по пути:
```
HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\WindowsRuntime\ActivatableClassId\Windows.Gaming.GameBar.PresenceServer.Internal.PresenceWriter
```
5. Правой кнопкой по папка
	- "Разрешения..."
	- Клик на группу "Администраторы"
	- "Допольнительно"
6. Рядом с первой строкой "Владелец" нажимаем "Изменить"
7. "Введите имена выбираемых объектов":
	- Имя пользователя виндовс (user/алёша/паша/zver и т.к.)
	- "Ок"
8. Меняем ключ "ActivationType" на значение `0`
---
## Хочу скачать ЛТСЦ с официанального сайта
1. Скачиваем [образ](https://microsoft.com/ru-ru/evalcenter/download-windows-10-enterprise)
2. Устанавливаем
3. [Конвертируем](https://github.com/victorlish/Convert_to_Windows_10_LTSC)
4. Активируем с [КМС](https://codeberg.org/shapkodebil/win10thread/#kms_vl_all-by-abbodi1406)

## Образы LTSC - IoT Enterprise LTSC

Добавил образ [`17763.3770`] и [`19044.3570`] на базе LTSC 2018 [`1809`] и IoT Enterprise LTSC 2021[`21H2`] с интегрированными обновлениями через [W10UI](https://github.com/abbodi1406/BatUtil/tree/master/W10UI):
> РЯЯЯЯЯЯЯЯЯЯЯ РАДМИНЫ ЗОНДЫ МАЙНЕРЫ! 
>> Всё,что попало под руку, я туда понапихал чтобы спиздить твой прон и аниме пак, на который ты дрочишь, держу в курсе, короче.

[19044.3570.230929-1700.21H2_RELEASE_SVC_PROD1_CLIENT_X64FRE_EN-US.iso](https://pixeldrain.com/u/ketx7LhJ) - `IoT Enterprise LTSC 2021`
- `CRC32: 9CA831F0`
- `CRC64: C2B28463E235E2BE`
- `SHA256: d8a2f2f3799af97033bb2a18913a5335b5c61c225ee8322794c4f8f8131b6b0b`
- `SHA1: 01dc375bbb6c076bac977ca39311fa933c84d11b`
- `BLAKE2sp: 5827d827c23865b46dc98310b9f38f2c718c054f6f720b4990948a017c26ca23`
- Интегрированные обновления:
	- [LCU KB5031356](https://support.microsoft.com/ru-ru/topic/10-октября-2023-г-kb5031356-сборки-ос-19044-3570-и-19045-3570-951fac64-5bf8-4eba-ba18-a9448920df1a)
	- [Out Of Box Experience (OOBE) Update ](https://support.microsoft.com/help/5020683)
	- [NDP481 Base](https://download.microsoft.com/download/2/7/8/278c0762-a0b1-41a7-b505-bf74715f3532/Windows10.0-KB5011048-x64.cab)
	- [Critical DU for .NET35](https://support.microsoft.com/help/5007401)
 	- [DU for SafeOS](https://support.microsoft.com/help/5031474)

[17763.3770.221209-2123.RS5_RELEASE_SVC_PROD1_CLIENT_X64FRE_RU-RU.iso](https://pixeldrain.com/u/RTvVybkn) - `LTSC 2018`
- `CRC32: 60E67A7A`
- `CRC64: C851F23F3034F001`
- `SHA256: 47f787a1546664e2f331fb055d21414d2d0d37b5a0fb12fb7fc362c8f9f53072`
- `SHA1: 40ebed3dc666066cd97334a89baf338640f3ad1e`
- `BLAKE2sp: a7e7d83353f74d0eda9c35348143c64b700279dd202f131591fa187a6c7012ea`
- Интегрированные обновления:
	- [LCU KB5021237](https://support.microsoft.com/en-us/topic/december-13-2022-kb5021237-os-build-17763-3770-8c1506cc-e030-4cf1-8cd6-774091f46f34)
	- [Secure Boot DBX Update x64 KB4535680](http://support.microsoft.com/kb/4535680)
	- [Secure Boot DBX Update](https://support.microsoft.com/help/5012170)
	- [Dotnet48 for RS3 16299, RS4 17134 and RS5 17763 KB4486153](http://support.microsoft.com/kb/4486153)
 	- [Dotnet48 LPs for RS3 16299, RS4 17134 and RS5 17763 KB4087642](https://support.microsoft.com/kb/4087642)
 	- [NDP35-48 KB5020874](https://support.microsoft.com/kb/5020874)
	- [DU for SafeOS](https://support.microsoft.com/help/5021042)
	- [DU for Sources KB5012162](https://support.microsoft.com/kb/5012162)


## Хочу установить библиотеки для ЛТСЦ чтобы устанавливать разные UWP приложения (Windows terminal)
1. Скачиваем [архив](https://www.upload.ee/files/14587097/ltsclibrariesuwp.7z.html)
	- Библиотеки брал [отсюда](https://forums.mydigitallife.net/threads/guide-add-store-to-windows-10-enterprises-sku-ltsb-ltsc.70741/page-30#post-1468779) удалил стор и xbox приложение, оставил только те библиотеки чтобы можно было устанавливать uwp приложения по двойному клику.
2. Распаковываем и запускаем от администратора "`Add-Libraries.cmd`"
3. Фсё, пробуем установить [терминал](https://github.com/microsoft/terminal/releases) например

---

## ОФИС ОФИС ОФИС ХАЧУ ЫЫЫЫЫЫ СРЕНЬК ПУУУУУУК
1. [tb.rg-adguard.net](https://tb.rg-adguard.net/public.php)
	- Новый домен как актуальная альтернатива -> [files.rg-adguard.net](https://files.rg-adguard.net/version/5f2ad9c6-e111-76e8-06d1-56d44c136bae) - (Ищем `"Office"`), так как `tb.rg-adguard.net` в будущем не будет больше поддерживатся.
2. Выбираешь:
- Microsoft Office
- Microsoft Office 2021
- Office 2021 Professional Plus
- %язык_который_нужен%
- %единственное_имя_файла.img%
3. Двойной клик по файлу и потом setup.exe
4. Пусть закончит установку
5. Активация `только` с [**KMS_VL_ALL**](https://codeberg.org/shapkodebil/win10thread/#kms_vl_all-by-abbodi1406) нажимаем `2`

[Общие сведения о средстве развертывания Office](https://learn.microsoft.com/ru-ru/deployoffice/overview-office-deployment-tool)

![](img/adguardoffice.png)


> **"Ээээм %номер_версий% КАЛ"**
>> Как скачивать уже знаешь просто выбери другую версию.

---

[Office(R)Tool](https://forums.mydigitallife.net/threads/office-r-tool-the-new-era.84450/)
Скачать через командную строку или повершелл от имени администратора:
```
#командная строка
PowerShell -nop -NoLogo -ExecutionPolicy bypass -EncodedCommand ^
SQBXAFIAIAAtAFUAcgBpACAAJwBoAHQAdABwAHMAOgAvAC8AbwBmAGYAaQBjAGUAcgB0AG8AbwBsAC4AbwByAGcALwBEAG8AdwBuAGwAbwBhAGQALwBHAGUAdABMAGEAdABlAHMAdABSACcAIAB8ACAASQBFAFgA

#повершелл
PowerShell -nop -NoLogo -ExecutionPolicy bypass -EncodedCommand `
SQBXAFIAIAAtAFUAcgBpACAAJwBoAHQAdABwAHMAOgAvAC8AbwBmAGYAaQBjAGUAcgB0AG8AbwBsAC4AbwByAGcALwBEAG8AdwBuAGwAbwBhAGQALwBHAGUAdABMAGEAdABlAHMAdABSACcAIAB8ACAASQBFAFgA

```

> **Note**
> **Office 2010:**
- [Образы 32/64](https://opendirectory.luzea.de/Enthousiast/Office/Office%20Pro%20Plus%202010%20SP1%20VL/)

---

1. [Образ](https://cloud.mail.ru/public/9KV2/mu3iZVBgb/2010/SP1/ProPlus/%40en_office_professional_plus_2010_with_sp1_x86_x64_dvd_730330.iso.rar)
2. [svf_1](https://cloud.mail.ru/public/9KV2/mu3iZVBgb/2010/SP1/ProPlus/)
3. [svf_2](https://cloud.mail.ru/public/9KV2/mu3iZVBgb/2010/SP1/ProPlus/VL)

Что такое ".svf" файл?:

`Это патч, который накладывается на твой оригинальный файл чтобы получить нужный тебе образ (например на русском или на другом языке и т.к.), майкрософт не распространяет образы виндовс со всеми языками (если было бы так то, вышла бы огромная хуета на много гигабайт, что непрактично) а просто предоставляет svf файлы и дефолтный образ, который потом просто интегрируется чтобы получить нужное.`

Для интеграции svf c iso чтобы создать образ на своём языке: [Smart Version](https://www.smartversion.com/smartvs382.zip)
Кидаем всё в одну папку и с командной строкой:

1. `.\smv x %имя_svf_1_файла% -br .`
2. `.\smv x %имя_svf_2_файла% -br .`

Следуем пунктам `3-4-5` в начале.
[Активируем](https://codeberg.org/shapkodebil/win10thread#kms_vl_all-by-abbodi1406)

[Модули,](https://archive.org/details/MSO2010) которые *надо* отключить из-за возможных проблем с безопасностью:

![](img/officedisable.png)


---
## Сборочки хуёрочки?
> **Warning**
> **`"А Я ТУТ НАШЁЛ СБОРОЧКУ ОТ ВАСЯНАВРОТЕБАЛ228 ОЙ МАМА ДОРОХАЯ КАКАЯ БЫСТРАЯ ВАЙЙЙЙ ПРЯМ ЛЕТАЕТ, СЛЫШ ПРЯМ ВСЁ ВЫРЕЗАНО И В ИГОРИ ПЛЮС ДВА ФПСА!"`**
>Пользоваться сборочками верх куколдства, васян туда понапихал что попало а ты это хлопаешь посмакивая и просишь ещё. Сборочки хуйня полная и всё что васян сделал можно сделать самому за несколько часов своего "драгоценного" времени блять.
---

## Активация
## KMS_VL_ALL (by abbodi1406)
Активирует все версии (volume) виндовс с 7 до 10:

  -  Windows 8 / 8.1 / 10 (Все официальные издания , кроме Windows 10 S)
  - Windows 7 (Корпоративная /N/E, Профессиональная /N/E, Embedded Standard/POSReady/ThinPC)
  - Windows Server 2008 R2 / 2012 / 2012 R2 / 2016 / 2019
  - Office Volume 2010 / 2013 / 2016 / 2019

Не используется интернет, планировщик задач, сервисы, серверы или драйвера. Код полностью открытый, ссылки находятся в readme.html в архиве полной версии. Работает на всех редакциях, которые можно активировать с помощью KMS. Вы можете активировать только Office с помощью KMS. Можно интегрировать и то, и другое, если требуется HWID для Windows и активация для Office, или использовать KMS для всего. 
> **Warning**
> Обязательно добавьте архив в исключения антивируса или временно отключите его.

![](img/kmsvlall.png)

**`Нажимаешь 2, и скрипт создаст задачу, которая будет обновлять активацию каждые 180 дней. После этого дополнительных действий с твоей стороны больше не потребуется.`**

> **Note**
> **[KMS_VL_ALL_AIO](https://github.com/abbodi1406/KMS_VL_ALL_AIO)** - ПАРОЛЬ: `"2023"`

> **Note**
> **Бэкап ссылки: [1](https://yzyveojj6tqzqwe5muzglg.on.drv.tw/KVA/ReadMe36.html),[2](https://forums.mydigitallife.net/posts/838808/),[3](https://pastebin.com/cpdmr6HZ),[4](https://textuploader.com/1dav8?__cf_chl_f_tk=xv7P1o994cZm7_6NCeyYTJ5MzaenzWHwTZZGjRKbj3c-1658049006-0-gaNycGzNB-U) - [Сурцы](https://yzyveojj6tqzqwe5muzglg.on.drv.tw/KVA/ReadMe36.html#Source)**

---

## Massgrave/MAS HWID
Активатор с открытым исходным кодом, который создает вечную цифровую лицензию, делая вид, что вы обновились со старой версии. Идентификатор железа отправляется на серверы Microsoft, и активация не должна сбрасываться после переустановки. Кроме того, если вы войдете в учетную запись Microsoft, она останется даже после замены оборудования. Активатор не может активировать старые версии Windows, Office и некоторые редакции. Он идеально подходит для нормальных пользователей, поскольку активация остается даже после удаления активатора или переустановки. Если вы планируете установить Office, рассмотрите вариант KMS_VL_ALL, так как иногда вам все равно придется иметь работающий KMS-сервер.

![](img/mas.png)

**`Нажимаешь 1, и Windows активируется навсегда.`**

> **Note**
> **[Microsoft-Activation-Scripts](https://github.com/massgravel/Microsoft-Activation-Scripts)**

> **Note**
> **Открываем `"Powershell"` от имени администратора и вставляем: `irm https://massgrave.dev/get | iex`**

 > **Note**
> **Бэкап ссылки: [1](https://bitbucket.org/WindowsAddict/microsoft-activation-scripts/src/master/), [2](https://massgrave.dev/#Method_2_-_Traditional)**

> **Note**
> **То же самое: [winactivate(luzea9903)](https://archive.org/details/github.com-luzeadev-winactivate_-_2021-11-19_17-07-04)**
 


**После активации активаторы и связанные с ними папки можно удалить; нет необходимости их хранить на диске.**

---

## Как сделать активированный образ
1. Скачиваем [MAS](https://github.com/massgravel/Microsoft-Activation-Scripts)
2. Открываем `All-In-One-Version\MAS_AIO.cmd`
3. Нажимаем `7` потом `2`
4. Выбираем метод `HWID` то есть `1`
5. Папку `$OEM$` которая появилась на рабочем столе копируем на флешку где уже записан образ, то есть:
	- `\sources\$OEM$`
7. Устанавливаем как обычно и при первом подключений к интернету она должна автоматически активироваться.

[сурцы](https://massgrave.dev/oem-folder.html)


## Скачать
- [Официальный сайт](https://www.microsoft.com/ru-ru/software-download/windows10/). Для скачивания ISO прямо с сайта нужно указать любой UA, кроме Windows:
	- Запускаем любой хром по вкусу
	- F12
	- CTRL SHIFT  M (Toggle device toolbar)
	- Смотришь вверх "Dimensions:" выбираешь любой
	- Перезагрузить вкладку
	- На самой страничке "выбрать выпуск"
	- "Windows 10 (multi-edition ISO)" и подтвердить

---

Чуть ниже находится Media creation tool (средство установки).
Если с Media Creation Tool на Windows 7 выдаёт ошибку `0x80072f8f-0x20000` то это значит что [протоколы TLS 1.1/TLS 1.2 не поддерживаются](https://support.microsoft.com/en-us/topic/update-to-enable-tls-1-1-and-tls-1-2-as-default-secure-protocols-in-winhttp-in-windows-c4bd73d2-31d7-761e-0178-11268bb10392#bkmk_easy) их можно включить с помощью официальной [утилитой от майкрософт](https://download.microsoft.com/download/0/6/5/0658B1A7-6D2E-474F-BC2C-D69E5B9E9A68/MicrosoftEasyFix51044.msi).

---

- [Оригинальные образы Windows MSDN *NNM-Club*](https://nnmclub.to/forum/viewforum.php?f=504/).
 **ОБРАЗЫ ЛТСЦ СРАЗУ СНИЗУ**
- [Оригинальные образы Windows MSDN *Rutracker*](https://rutracker.org/forum/tracker.php?f=2489&nm=Windows10).
- Оригинальные образы Windows MSDN *Rustorka*:
	- [22H2](https://rustorka.com/forum/viewtopic.php?t=284231)
	- [21H2 LTSC](https://rustorka.com/forum/viewtopic.php?t=272933)

---

- [Репозитория](https://pastebin.com/raw/SF77qXtg) оффициальных ESD(Electronic Software Distribution) файлов для конвертаций в ISO
	1. Скачиваем нужный нам образ (например `19045.2006.220908-0225.22h2_release_svc_refresh_CLIENTBUSINESS_VOL_x64FRE_ru-ru.esd`)
	2. Скачиваем [esd-decrypter-wimlib](https://github.com/abbodi1406/BatUtil/tree/master/esd-decrypter-wimlib)
	3. Кидаем всё в одну папку 
	4. Перетягиваем `.esd` файл на `decrypt.cmd`

---

- [Pastebin с разными ссылками](https://pastebin.com/raw/MDQS8Gve)
	- [stuff.mtt-m1.workers.dev](https://stuff.mtt-m1.workers.dev)
	- [isofiles.bd581e55.workers.dev](https://isofiles.bd581e55.workers.dev)

---

- [Rg-adguard](https://files.rg-adguard.net/version/f0bd8307-d897-ef77-dbd6-216fefbe94c5). Прямые ссылки с сайта Microsoft.com. Любая виндовс и офис.
	- [Windows 10, version 22H2 (Updated Jan 2023)](https://files.rg-adguard.net/language/2035a908-5306-b748-0b90-cf987ca2dbae)
Ждём пока вся платформа пропердится да и собственно сиды были.

- [Музей официальных образов](https://nnmclub.to/forum/viewforum.php?f=916/). Есть даже 1507, самая первая версия, такое интересно будет посмотреть в виртуальной машине.
> **Warning**
> Всегда сверяй с дампами хешей
## Дампы хешей

- [MVS dump](https://awuctl.github.io/mvs/)
	- **Нажимаем на "Select products" и вводим, к примеру, "Windows 10 22H2". Затем нажимаем клавишу Enter, чтобы выбрать нужную версию. Переходим к разделу "Generate summary files" и просто нажимаем "Generate". Теперь мы можем скачать файл со всеми хэшами.**

- [sha1.rg-adguard](https://sha1.rg-adguard.net/)

- [myvsdump](https://heidoc.net/php/myvsdump.php)

- [mvs-dump](https://forums.mydigitallife.net/threads/mvs-dump.78070/) - **Нужен аккаунт**

## Как создать загрузочный образ?

- [Rufus](https://rufus.ie/) (свободное ПО) самый простой и удобный способ.
	- Выбираешь схему раздела MBR для старых компьютеров без UEFI и GPT для новых. Можно нажать AltE, и флешка подойдет для обоих случаев, что удобно при установке на разные компьютеры. Однако будь осторожен и не устанавливай Windows в режиме Legacy на современном компьютере. Убедись, что в BIOS установлен режим UEFI Only.

- [Ventoy](https://github.com/ventoy/Ventoy) Тебе не нужно повторно форматировать диск, просто скопируй образы на USB-накопитель и загрузись с него.

## Полезное
>Всё на ваше усмотрение. Я добавляю ссылки, которые касаются операционной системы Windows. Если захотите, можете почитать и узнать об этом. Если нет, вы смело можете пройти мимо - ваша система будет работать как и прежде.
Основные компоненты при новой установки виндовс:

- [Visual C](https://github.com/abbodi1406/vcredist)
- [.NET Framework 4.8.1](https://dotnet.microsoft.com/en-us/download/dotnet-framework/thank-you/net481-offline-installer)
- [DirectX End-User Runtimes (June 2010)](https://www.microsoft.com/en-us/download/confirmation.aspx?id=8109)

---

- [GoodbyeDPI](https://github.com/ValdikSS/GoodbyeDPI) - Для разблокировки сайтов без VPN и прокси, можно установить как службу.
- [Deployment Tools](https://docs.microsoft.com/en-us/windows/deployment/windows-deployment-scenarios-and-tools)
- [Старый гайд для optimize-offline](https://rentry.co/mdl-optimize-guide)
- [Групповые политики для домашний версий](https://github.com/Fleex255/PolicyPlus)
- [Возможность скачивания приложений стора без стора, устанавливать через повершелл](https://store.rg-adguard.net/)
- [Powertoys](https://github.com/microsoft/PowerToys/releases)
- [Windows Terminal](https://github.com/microsoft/terminal/releases)
- [Icaros](https://shark007.net/forum/Thread-New-Release-3-3-0-RC-1?pid=689#pid689), [2](https://github.com/Xanashi/Icaros/releases)
Для эскизов `.webm` файлов
- [Office Retail-to-Volume](https://github.com/abbodi1406/C2R-R2V-AIO/blob/master/C2R-R2V-AIO.cmd)
- [Мануал с GHacks по переносу лицензии с до-десяток на десятку](https://www-ghacks-net.translate.goog/2015/08/30/how-to-clean-install-windows-10-directly-without-upgrade/?_x_tr_sl=auto&_x_tr_tl=ru&_x_tr_hl=ru)
- [AMD Chipset](https://www.amd.com/en/support)
- [Недооптимизатор WinAero Tweaker](https://winaero.com/downloads/winaerotweaker.zip)
- [КулРидер](https://fb2-reader.ru/read/cool_reader_3) - Настройка шрифтов и самого "пергамента". Возможно имитировать старинный свиток или что-то в этом духе.

Образы, ключи и скрипты:
- [Дамп разных ключей(возможно не работают)](https://jike.info/topic/2631/win-10-rtm-professional-retail-oem-mak?lang=en-US&page=1)
- [Много очень полезных скриптов от abbodi1406](https://github.com/abbodi1406/BatUtil)
- [BloatRemover(твикер) на свой страх и риск особо не рекомендую](https://github.com/Fs00/Win10BloatRemover)
- [Разная информация об образов и т.к](https://massgrave.dev/readme-genuine-installation-media.html)

Старый стиль win7:
- [Старый калькулятор](https://winaero.com/get-calculator-from-windows-8-and-windows-7-in-windows-10/)
- [Open-Shell(Старый Пуск)](https://github.com/Open-Shell/Open-Shell-Menu)
- [Старый микшер](https://vynesimozg.com/kak-vernut-staryj-regulyator-gromkosti-v-windows-10/)
- [Старое средство просмотра фотографий](https://remontka.pro/photo-viewer-windows-10/)
- [OldNewExplorer Разные допольнительные настройки для проводника](https://www.majorgeeks.com/files/details/oldnewexplorer.html)

Обновления:
- [Пакеты обновлений](https://support.microsoft.com/en-us/topic/windows-10-update-history-857b8ccb-71e4-49e5-b3f6-7073197d98fb)
- [Пакеты безопасности для корректного установление пакетов обновления ](https://msrc.microsoft.com/en-us/security-guidance/advisory/ADV990001)
- [WHDownloader](https://forums.mydigitallife.net/threads/whdownloader-download.66243/), [2](https://www.mediafire.com/file/ootxxrbhw73wt4h/WHDownloader_0.0.2.4.zip/file), [3](https://drive.google.com/file/d/1hIZbPDjK4sTADQ0oFdEsAa7MRC-hQbAC/view?usp=sharing)
- [Утилита для загрузки обновлений вручную "WSUS offline"](https://gitlab.com/wsusoffline/wsusoffline)

Разное для лтсц:
- [Стор на лтсц нужен аккаунт](https://forums.mydigitallife.net/threads/guide-add-store-to-windows-10-enterprises-sku-ltsb-ltsc.70741/)
- [Стор на лтсц не обновлённый](https://github.com/kkkgo/LTSC-Add-MicrosoftStore)
- [Конвертация LTSC Evaluation на LTSC](https://github.com/victorlish/Convert_to_Windows_10_LTSC)

Office 2010 и прочее:
- [Office 2010](https://archive.org/details/MSO2010), [2](https://forums.mydigitallife.net/threads/office-2010-professional-plus-sp1-iso-volume-37-languages.56883/), [3](https://opendirectory.luzea.de/Enthousiast/Office/Office%Pro%Plus%2010%SP1%VL/), [4](https://cloud.mail.ru/public/9KV2/mu3iZVBgb/2010)
- [Разное для офиса](https://github.com/YerongAI/Office-Tool)
- [Старые ссылки на офис](https://pastebin.com/raw/PLhB7UnK)
- [Исправление ошибки "копия офиса не легитимна"](https://massgrave.dev/office-license-is-not-genuine)
- [Ohook активация](https://github.com/asdcorp/ohook)

Всё для нвидий и амд драйверов видеокарты:
- [Утилита для полного удаление через безопасный режим старых драйверов видеокарты](https://www.guru3d.com/files-details/display-driver-uninstaller-download.html)
- [Утилита для установки только необходимых компонентов драйверов нвидий](https://www.techpowerup.com/nvcleanstall/)
- [Утилита для отключения Ansel Nvidia](https://nvidia.custhelp.com/app/answers/detail/a_id/4932)
- [Утилита для установки только необходимых компонентов драйверов амд](https://github.com/GSDragoon/RadeonSoftwareSlimmer)

## ПО по вкусу
>Ты чё за хуйню рекомендуешь э ёба? Ты знаешь что *%ПРОГРАММА_НЕЙМ%* лучше???
>>Сразу идёшь нахуй мне похуй, есть внятные предложения почему она лучше то пости в треде без флуда и я обновлю а так я скопирую нормальную [пикчу](https://s1.desu-usergeneratedcontent.xyz/g/image/1562/46/1562467997749.png) от *друзей* с форча

**Браузер:**
[Firefox](https://ftp.mozilla.org/pub/firefox/releases/) `10x.x.x\` - [Firefox ESR](https://ftp.mozilla.org/pub/firefox/releases/) `Ищем с приставкой "esr\"` - [Ungoogled Chromium 1](https://ungoogled-software.github.io/ungoogled-chromium-binaries/releases/windows/64bit/) - [Ungoogled Chromium 2](https://github.com/macchrome/winchrome/releases)

**Текстовый редактор:**
[Notepad ](https://notepad-plus-plus.org/downloads/) - [Vscodium](https://github.com/VSCodium/vscodium) - [Sublime Text](https://www.sublimetext.com/download) ([Делаем себе сами через hex бесконечную лицензию для sublime](https://gist.github.com/maboloshi/feaa63c35f4c2baab24c9aaf9b3f4e47))

**Торрент-клиент:**
[qBittorent](https://www.qbittorrent.org/download.php) - [Transmission](https://transmissionbt.com/download/) - [Deluge](https://ftp.osuosl.org/pub/deluge/windows/?C=M;O=D) - [PicoTorrent](https://picotorrent.org/download)

**Просмотр изображений:**
[nomacs](https://github.com/nomacs/nomacs) - [jpegview](https://github.com/sylikc/jpegview) - [pictus](https://github.com/poppeman/Pictus/releases)

**Видео-Конвертер:**
[FFmpeg](https://ffmpeg.org/download.html#build-windows) - [Handbrake](https://handbrake.fr/downloads.php) - [StaxRip](https://github.com/staxrip/staxrip/releases)

**Видеоплееры:**
[MPC-HC](https://github.com/clsid2/mpc-hc/releases) - [mpv](https://sourceforge.net/projects/mpv-player-windows/files/) - [mpv.net](https://github.com/stax76/mpv.net/releases) - [VLC](https://www.videolan.org)

**Музыкальный Проигрыватель:**
[Clementine](https://www.clementine-player.org/ru/downloads) - [foobar2000](https://www.foobar2000.org/download) - [Audacious](https://audacious-media-player.org/download) - [Aimp](https://www.aimp.ru/?do=download)

**Запись Экрана:**
[OBS](https://obsproject.com/ru/download)

**Шифровка данных:**
[Veracrypt](https://www.veracrypt.fr/en/Downloads.html)

**Управление файлами**
[7zip](https://www.7-zip.org/download.html) - [fman](https://fman.io/download) - [Everything](https://www.voidtools.com/ru-ru/downloads/)

**Просмотр PDF:**
Твой браузер - [SumatraPDF](https://www.sumatrapdfreader.org/download-free-pdf-viewer)

**Офис:**
[Google Документы](https://docs.google.com/) - [Libre Office](https://www.libreoffice.org/download/download/?lang=ru)

**Общение:**
[Pidgin](https://www.pidgin.im) - [Thunderbird](https://www.thunderbird.net/ru/) - [Mumble](https://www.mumble.info/downloads/)

**Клинер:**
[Bleachbit](https://www.bleachbit.org/download/windows)


## Установка Windows 10
> **Note**
> [windows_for_retards](https://rentry.co/windows_for_retards)

>Гайд для полных даунов
## Что будет нужно
- Интернет
- Система хотя бы на семёрке для создания образа
- Флешка на хотя бы 8 гигов
- [Торрент клиент](https://www.qbittorrent.org/download.php)
- Комп с поддержкой UEFI
## Что получишь в конце
- Десятка Образовательная

Если захочешь:
- Полунапердолленая система без поломок
## Скачиваем виндовс
1. Открываешь [ru-ru_windows_10](https://nnmclub.to/forum/viewtopic.php?t=1662987)
2. Жмёшь на магнит
3. Скачиваешь только ru-ru_windows_10_business_editions_version_22h2_updated_sep_2023_x64_dvd_3e14a3b2.iso

![](img/nnmclubwiniso.png)

## Скачиваешь Ventoy
1. Открываешь [Ventoy](https://github.com/ventoy/Ventoy/releases)
2. Скачиваешь `"ventoy-1.0.XX-windows.zip"`
3. Распаковываешь в любое удобное для тебя место (Распаковать можно например с [7zip](https://www.7-zip.org/download.html))

![](img/ventoy.png)

## Запись образа на флешку
1. Вставляешь флешку
2. Открываешь `"Ventoy2Disk.exe"`
3. `"Устройство"` выбираешь свою флешку
4. Нажимаешь "Установить"
5. Когда установка закончится просто перетягиваем наш образ `.iso` на нашу флешку с названием `"Ventoy"` (Например `(E:) Ventoy`)

![](img/ventoycopy.png)

## Загрузка с флешки для дальнейший установки
- [x] Берёшь гуглишь как на твоей **марки ноутбука/материнки** запустится с флешки и так далее в основном ищешь какую **кнопку** жать при запуске компа

![](img/biosbutton.png)

> **Warning**
> Может быть что угодно `"Del" - "F2" - "F8"` и т.к.

---

## Сама установка
1. `"Устанавливаемый язык"`

2. `"Формат времени и денежных единиц"`

3. `"Метод ввода"`

![](img/1windows.png)

4. **"Ключ продукта":**
- [x] **`"У меня нет ключа продукта" `**

![](img/2windows.png)

5. **"Выберите ос, которую вы хотите установить"**
- [x] **`Windows 10 для образовательных учреждений`**

![](img/3windows.png)

6. **"Условия лицензии"**
- [x] **`Я принимаю условия лицензии`**

![](img/4windows.png)

7. **"Выберите тип установки"**
- [x] **`Выборочная`**
> **Warning**
> **Обязательно сделайте бэкап всех данных перед очисткой диска или раздела.**

![](img/5windows.png)

8. **"Где вы хотите установить Windows?":**
Выбираем `Незанятое пространство`, или `удаляем старый раздел`. Если выбрать раздел со старой виндовс, она переместится в папку **Windows.old** со всеми файлами. Хочешь удалить [всё](https://codeberg.org/shapkodebil/win10thread#хочу-перед-установкой-виндовс-очистить-диск-от-всего-и-ещё-поменять-структуру-раздела) что бы наверняка?

![](img/6windows.png)

> **Note**
> **Ждёшь окончание установки.**

---

## Конфигурация (OOBE)
> **Note**
> **Очень рекомендую отключить нахуй интернет (кабель и т.п.) во время всего процесса установки что бы избежать проблем с доёбыванием майкрософта с аккаунтом и разными обновлениями (особенно драйверами видеокарты) и собственно *телеметрий*(если это вас ебёт), включайте обратно когда уверенны что система сконфигурирована правильно и по вашему вкусу или надо активировать через HWID.**
1. Выбери - Регион и раскладку

![](img/oobe1.png)

![](img/oobe2.png)

2. "Давайте подключим вас к сети":

- [x] **`"У меня нет Интернета"`**

![](img/oobe3.png)

- [x] **`"Продолжить ограниченную установку"`**

![](img/oobe4.png)

3. Выбери - имя пользователя

![](img/oobe5.png)

4. "Выберите параметры конфиденциальности для этого устройства":
- **`Отключить нахуй "Диагностические данные" - "Реклама" всё остальное на своё усмотрение`**

![](img/oobe6.png)

---

## Активация самой виндовс
1. Скачиваешь любой из активаторов
2. Скачиваешь архиватор [7zip](https://www.7-zip.org/download.html)
3. Отключаешь на время и разрешаешь архив/exe активатора в **антивирусе** (Microsoft Defender)

Два метода [активаций](https://codeberg.org/shapkodebil/win10thread/#активация) по вкусу:

- [**HWID**](https://codeberg.org/shapkodebil/win10thread/#massgravemas-hwid):
	1. "Запустить от имени администратора"
	2. Нажимаешь 1
	`Вариант если просто нужно активировать виндовс`

- [**KMS_VL_ALL**](https://codeberg.org/shapkodebil/win10thread/#kms_vl_all-by-abbodi1406):
	1. "Запустить от имени администратора"
	2. Нажимаешь 2
	`Вариант если будет нужен офис`

> **Note**
> **LTSC IoT Enterprise 2021 [21H2] - LTSC 2018 [1809] *МОЖНО* активировать с HWID *ИЛИ* KMS_VL_ALL.**

> **Note**
> **LTSC 2021 [21H2] *ТОЛЬКО* с KMS_VL_ALL.**

> **Note**
> **HWID активирует навсегда и нужен интернет, KMS_VL_ALL будет обновлять каждые 180 дней по собственному оффлайн серверу (а ля эмуляция сервера майкрософта) на твоей машине, никакого интернета не надо и не будет нужно.**

---

## Хочу как следует попердолить сломать мою систему
1. Скачиваешь [Privatezilla](https://github.com/builtbybel/privatezilla)
2. Распаковываешь
3. Оставляешь по дефолту все настройки да бы не разьебать всё нахуй
 "Disable Windows Hello Biometrics" не даст камере или отпечатку работать нормально как и пароли, для ноутбука лучше не отключать.
4. "Применить" - "Apply selected"

![](img/privatezilla.png)

---

by *шапко-дебил*

![](img/shapkodebil.png)

---
:warning:

[Первоначальная шапка на rentry.co](https://rentry.co/win10thread)

[Старый faq](https://github.com/w10dd/op/wiki)
